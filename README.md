# Learn ISO-8601

The [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard establishes
conventions for expressing dates and times so that programs can agree about how
to represent them, especially during transport. The standard, however, is long
and complicated, and library support for it across languages is often terrible.
Learn ISO-8601 is an effort to help programmers learn the ins and outs of the
standard and how to work with ISO-8601 formatted strings in code.


## Usage

Pick a language you want to work in. It should read from `STDIN`. For each line
that comes in via `STDIN`, it should do an appropriate transformation, then
output the transformed data on `STDOUT`. To know what transformation needs to be
done, submit a wrong answer and Learn ISO-8601 will guide you.

For instance, if you were working in ruby, and were going to do your work in a
file named `iso8601.rb`, you'd do like this:

```
$ cat inputs | iso8601.rb | learn_iso_8601.exs
```

There are no rules against looking up documentation or using your language's
standard library for dealing with time (though you may find it incomplete). The
important thing is learning.


## Contributing

If you'd like to contribute, please see the
[contribution guidelines](CONTRIBUTING.md).


## Releasing

Maintainers: Please make sure to follow the [release steps](RELEASING.md) when
it's time to cut a new release.
