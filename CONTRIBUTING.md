# Contributing

Help is gladly welcomed. If you have a feature you'd like to add, it's much more
likely to get in (or get in faster) the closer you stick to these steps:

1. Open an Issue to talk about it. We can discuss whether it's the right
   direction or maybe help track down a bug, etc.
1. Fork the project, and make a branch to work on your feature/fix. Master is
   where you'll want to start from.
1. [Create a Merge Request](https://gitlab.com/benhamill/learn_iso_8601/merge_requests/new)
   for your change and make sure to link the relevant Issue by mentioning the
   number in the Merge Request description.
1. Make sure your Merge Request includes tests.
1. Bonus points if your Merge Request updates [CHANGELOG.md](CHANGELOG.md) to
   include a summary of your changes and your name like the other entries. If
   the last entry is the last release, add a new `## Unreleased` heading.
1. *Do not* rev the version number in your pull request.

If you don't know how to fix something, even just a Merge Request that includes
a failing test can be helpful. If in doubt, make an Issue to discuss.
