# Releasing

If you want to push a new version of this gem, do this:

1. Ideally, every Merge Request should already have included an addition to the
   [CHANGES.md](CHANGES.md) file summarizing the changes and crediting the
   author(s). It doesn't hurt to review this to see if anything needs adding.
1. Commit any changes you make.
1. Go into [mix.exs](mix.exs) and bump the version number
   [as appopriate](http://semver.org/).
1. Go into [CHANGES.md](CHANGES.md) and change the `## Unreleased` heading to
   match the new version number.
1. Commit these changes with a message like, "Minor version bump," or similar.
1. Run `mix release`.
1. High five someone nearby.
